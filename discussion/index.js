// [SECTION] While Loop

// If the condition evaluates to true, the statements inside the code blcok will executed

/*
	Syntax 
	while (expression/condition){
		statement
	}
*/

// While the value of count is not equal to 0

// While the value of count is not equal to 0
// iteration - it increases the value of count after every iteration to stop the loop when it reaches 5

	// ++ - increment, -- decrement

let count=5;

while(count !==0){

	console.log("while:" + count)

count --;
}

// Do while loop
// A do-while loop works a lot like the while loop. But unlike while loops, do-while loops guarantee that the code wwill be executed at least once. 

/*

Syntax:

do{
	statement
}while (expression/condition)

*/

let number = Number(prompt("Give me a number"));

do{
	console.log("Do While: "+ number);
number += 1;

}
while (number<10)


// For loops
// A for loop is more flexible than while and do while

/*
Syntax
for(initialization. expressiion/condition;finalExpression){
	statement
}

*/

for (let count = 0; count <=20; count++){
	console.log(count);
}


let myString = prompt("give me a word");
// Characters in strings may be counted using the .length property.


// console.log(myString.length);

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);

// Will create a lopop that will print out the individual letters of a variable

for (let x = 0; x < myString.length; x++){

	// The current value of my string is printed out using it's index value
	console.log(myString[x])
}


// Changing of vowels using loops

let myName = "DelIZo";

for (let i = 0; i < myName.length; i++){
	if (
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "u" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "o" 
	){
		console.log(3);
	} else {
		console.log(myName[i]);
	}
}







































